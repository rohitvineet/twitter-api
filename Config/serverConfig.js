'use strict';

const PORT = {
	DEV: 8000
};

const JWT_SECRET_KEY = "BbZJjyoXAdr8BUZuiKKARWimKfrSmQ6fv8kZ7OFfc";
const APP_NAME = 'Twitter API';

module.exports = {
	JWT_SECRET_KEY: JWT_SECRET_KEY,
	APP_NAME: APP_NAME,
	PORT: PORT
};