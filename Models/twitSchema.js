'use strict';

const mongoose =require('mongoose');
const moment = require('moment-timezone');
const CONST = require('../Config');
const Schema = mongoose.Schema;

const tweetSchema = new Schema({
	username: {type: String, required: true},
	tweetContent: {type: String}
},
{timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }}
);

module.exports = mongoose.model('tweet',tweetSchema,'tweet');