'use strict';
const jwt = require('jsonwebtoken');
const jwtDecode = require('jwt-decode');
const UserModel = require('../Models/userModel');  
const tweetModel = require('../Models/twitSchema');       
const Joi =  require('joi');
const async = require('async');
const Config = require('../Config/serverConfig');
const Plugins = require('../Plugins/');

let authorizeHeaderObject = Joi.object({
	authorization: Joi.string().required()
}).unknown();

const secret = Config.JWT_SECRET_KEY;
const getRequest = {
					method:'POST',  
					path:'/login',           //url
					config:{
						tags:['api'],                //tags enable swagger to documnet API
						description:'Get all User Data',
						notes:'Get all user Data',
						validate:{
						payload:{    
							username:Joi.string().required(),
							password:Joi.string().required()
						}
					}
					},
					handler:function(request,reply){
							
							async.waterfall([
								function(callback){
									UserModel.findOne({$or:[{username:request.payload.username},{email:request.payload.username}]},function(err,user){
									if(err)
										throw err;
									if(!user)
										callback(null,"UNF");
									else
										callback(null,user);
								});
								},
								function(arg,callback){
									if(arg==="UNF")
										callback(null,"UNF");
									else
										arg.comparePassword(request.payload.password,function(err,isMatch){
											if(err)
												throw err;
											else if(isMatch)
											{
												 let token = jwt.sign({username:request.payload.username},secret,{expiresIn:1440});

												callback(null,token);
											}
											else
												callback(null,false);
									});
								}
								],
								function(err,res){
									if(res=="UNF")
										reply({message:"User not found!!"});
									else if(res==false)
										reply({message:"Login Failed!!"});
									else 
										reply({statusCode:200,message:"Login Successfull!!",data:res});
									}
								);
						}
						};


const postRequest ={
	method:'POST',
	path:'/register',
	config:{
		tags:['api'],
		description:'Save User data',
		notes:'Save user Data',
		validate:{
			payload:{
				names:Joi.string().required(),       //these are paramters we get through FORM body
				email:Joi.string().required(),
				username:Joi.string().required(),
				password:Joi.string().required(),
				contact:Joi.number(),
				gender:Joi.string(),
				age:Joi.number()
			}
		}
	},
	handler:function(request,reply)
	{
		var user = new UserModel(request.payload);
		user.save(user,(err)=>{
			
			if(err)
			{
				reply({
					statusCode:503,
					message:'User not inserted'
				});
			}
			else
			{
				reply({
					statusCode:201,
					message:'User is inserted'
				});
			}
		});
	}
};

const deleteUser = {
	method:'POST',
	path:'/deluser',
	config:{
		tags:['api'],
		description:'Delete user',
		notes: 'delete user',
		validate:{
			headers: authorizeHeaderObject,
			payload: {
				username: Joi.string().required()
			}
	  },
	  auth:{
	  	strategy:'token'
	  }
	},
	handler:function(request,reply){
		UserModel.remove({username:request.payload.username},function(err,user){
			if(err)
				throw err;
			else
			{
					reply({
						message:"User deleted!!"});
			}
		});
}
};

const tweet = {
	method: 'POST',
	path: '/tweet',
	config:{
		tags:['api'],
		description:'tweet',
		notes: 'tweet',
		validate:{
			headers: authorizeHeaderObject,
			payload: {
				tweetContent: Joi.string().required()
			}
	  },
	  auth:{
	  	strategy:'token'
	  }
	},
	handler:function(request,reply){
		var decoded = jwtDecode(request.headers.authorization);
		var data={
			username: decoded.username,
			tweetContent: request.payload.tweetContent
		};
		var tweet = new tweetModel(data);
		tweet.save(tweet,function(err){
			if(err)
			{
				reply({
					statusCode:503,
					message:'Failed to tweet!!'
				});
			}
			else
			{
				reply({
					statusCode:201,
					message:'Tweeted!!!'
				});
			}
		});
	}
};


const tweetByUser = {
	method: 'POST',
	path: '/tweetByUser',
	config:{
		tags:['api'],
		description:'tweetByUser',
		notes: 'tweetByUser',
		validate:{
			headers: authorizeHeaderObject
	  },
	  auth:{
	  	strategy:'token'
	  }
	},
	handler:function(request,reply){
		var decoded = jwtDecode(request.headers.authorization);
		tweetModel.find({username: decoded.username},{tweetContent:1, _id:0},function(err,res){
			if(err)
			{
				reply({
					statusCode:503,
					message:'No Content to show!!'
				});
			}
			else
			{
				reply({
					statusCode:201,
					data:res
				});
			}
		});
	}
};

const followUser = {
	method: 'POST',
	path: '/followUser',
	config:{
		tags:['api'],
		description:'followUser',
		notes: 'followUser',
		validate:{
			headers: authorizeHeaderObject,
			payload: {
				username: Joi.string().required()
			}
	  },
	  auth:{
	  	strategy:'token'
	  }
	},
	handler:function(request,reply){
		var decoded = jwtDecode(request.headers.authorization);
		console.log(request.payload.username);
		UserModel.update({username: decoded.username},{'$push': {following: request.payload.username}},function(err,res){
			if(err)
			{
				reply({
					statusCode:503,
					message:'Error in following!!!'
				});
			}
			else
			{
				UserModel.update({username: request.payload.username},{'$push': {followed: decoded.username}},function(err,res){
				if(err)
					reply({
					statusCode:503,
					message:'Error in following!!!'
					});
				else
					reply({
						statusCode:201,
						message:'Followed @'+request.payload.username
					});
				});
			}
		});
	}
};

const unfollowUser = {
	method: 'POST',
	path: '/unfollowUser',
	config:{
		tags:['api'],
		description:'unfollowUser',
		notes: 'unfollowUser',
		validate:{
			headers: authorizeHeaderObject,
			payload: {
				username: Joi.string().required()
			}
	  },
	  auth:{
	  	strategy:'token'
	  }
	},
	handler:function(request,reply){
		var decoded = jwtDecode(request.headers.authorization);
		console.log(request.payload.username);
		UserModel.update({username: decoded.username},{'$pull': {following: request.payload.username}},function(err,res){
			if(err)
			{
				reply({
					statusCode:503,
					message:'Error in unfollowing!!!'
				});
			}
			else
			{
				UserModel.update({username: request.payload.username},{'$pull': {followed: decoded.username}},function(err,res){
				if(err)
					reply({
					statusCode:503,
					message:'Error in unfollowing!!!'
					});
				else
					reply({
						statusCode:201,
						message:'Unfollowed @'+request.payload.username
					});
				});
			}
		});
	}
};



module.exports = [
	getRequest,postRequest,deleteUser,tweet,tweetByUser,followUser,unfollowUser
];
