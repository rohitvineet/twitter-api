'use strict';
const Inert = require('inert');
const Vision = require('vision');
const pack = require('../package');
const swaggerOptions = { 
	info:{
		'title':'Twitter API',       //showing this information on swagger  //localhost:port_number/documentation
	}
};

var Swagger = {
        register: require('hapi-swagger'),
        options: swaggerOptions
    };

exports.register = function(server, options, next){

    server.register(
    	[ Inert, Vision,Swagger], function (err) {
        if (err) {
            server.log(['error'], 'hapi-swagger load error: ' + err);
        }else{
            server.log(['start'], 'hapi-swagger interface loaded');
        }
    });

    next();
};

exports.register.attributes = {
    name: 'swagger-plugin'
};