'use strict';
const Config = require('../Config/serverConfig');
const User = require('../Models/userModel');
const privateKey= Config.JWT_SECRET_KEY;

function validate(decodedToken,request,callback){
	User.findOne({username:decodedToken.username},function(err,user){
		if(err)
			throw err;
		console.log(decodedToken);
		let error,credentials = user.username || {};
		if(!credentials)
			callback(null,false,credentials);
		callback(null,true,credentials);
	});
};


exports.register = function(server, options, next){


    server.register(require('hapi-auth-jwt2'),(err)=>{
			
	server.auth.strategy('token','jwt',{
	    
			key : privateKey,
			validateFunc : validate,
			verifyOptions:{ algorithms:['HS256'] }
	});
	
    });
    
    next();
};

exports.register.attributes = {
    name: 'hapi-jwt-plugin2'
};