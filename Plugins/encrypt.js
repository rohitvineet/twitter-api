'use strict';

const Config = require('../Config/serverConfig');   
const privateKey = Config.JWT_SECRET_KEY;
const jwt = require('jsonwebtoken');

const validate = function (request, decodedToken, callback) {
    const error,
        credentials = accounts[decodedToken.accountId] || {};
    if (!credentials) {
        return callback(error, false, credentials);
    }
    return callback(error, true, credentials)
};

exports.register = function(server, options, next){
server.register(require('hapi-auth-jwt'), function (error) {
    server.auth.strategy('token', 'jwt', {
        key: privateKey,
        validateFunc: validate,
        verifyOptions: { algorithms: [ 'HS256' ] }  // only allow HS256 algorithm
    });
});
};

exports.register.attributes = {
    name: 'hapi-auth-jwt-plugin'
};
